import * as THREE from 'https://threejsfundamentals.org/threejs/resources/threejs/r115/build/three.module.js';
import {FirstPersonControls} from 'https://threejsfundamentals.org/threejs/resources/threejs/r115/examples/jsm/controls/FirstPersonControls.js';

let scene, camera, renderer, container, controls;
let W, H;
let light;



let Orbit = function (radius){
    this.radius=radius;

    this.draw=function (scene){
        let og= new THREE.Geometry();
        let om = new THREE.PointsMaterial({
            transparent: true,
            color: 0x808080,
            opacity: 0.3,
            size: 1,
            sizeAttenuation: false});

        for (let i = 0; i < 500; i++) {
            let vertex = new THREE.Vector3();
            vertex.x = Math.sin(180/Math.PI  * i) *this.radius ;
            vertex.z = Math.cos(180/Math.PI * i) *this.radius ;
            og.vertices.push(vertex);
        }
        let obj = new THREE.Points(og, om);
        scene.add(obj);
    };
};

W = parseInt(window.innerWidth);
H = parseInt(window.innerHeight);

container = document.createElement('div');
document.body.appendChild(container);

camera = new THREE.PerspectiveCamera(45, W / H, 1, 1000000);
camera.position.z = 100000;
camera.position.y=10000;
scene = new THREE.Scene();

controls = new FirstPersonControls(camera);
controls.movementSpeed = 70000;
controls.lookSpeed = 1.6;


renderer = new THREE.WebGLRenderer({alpha: true, antialias: true});
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.PCFSoftShadowMap;
renderer.setSize(W, H);
container.appendChild(renderer.domElement);


light = new THREE.PointLight(0xffffff, 1.7, 200000);
light.position.set(0, 0, 0);
light.castShadow = true;
light.shadow.mapSize.width = 2048;
light.shadow.mapSize.heigth = 2048;
scene.add(light);


//stars
let starsGeometry = new THREE.Geometry();
let starsMaterial = new THREE.PointsMaterial({
    transparent: true,
    color: 0xbbbbbb,
    opacity: 0.6,
    size: 1,
    sizeAttenuation: false
});
let stars;

for (let i = 0; i < 15000; i++) {
    let vertex = new THREE.Vector3();
    vertex.x = Math.random() * 2 - 1;
    vertex.y = Math.random() * 2 - 1;
    vertex.z = Math.random() * 2 - 1;
    vertex.multiplyScalar(6000);
    starsGeometry.vertices.push(vertex);
}

stars = new THREE.Points(starsGeometry, starsMaterial);
stars.scale.set(50, 50, 50);
scene.add(stars);

let starsGeometry2 = new THREE.Geometry();
let starsMaterial2 = new THREE.PointsMaterial({
    transparent: true,
    color: 0xbbbbbb,
    opacity: 1,
    size: 1,
    sizeAttenuation: false
});
let stars2;

for (let i = 0; i < 3000; i++) {
    let vertex = new THREE.Vector3();
    vertex.x = Math.random() * 2 - 1;
    vertex.y = Math.random() * 2 - 1;
    vertex.z = Math.random() * 2 - 1;
    vertex.multiplyScalar(6000);
    starsGeometry2.vertices.push(vertex);
}

stars2 = new THREE.Points(starsGeometry2, starsMaterial2);
stars2.scale.set(70, 150, 100);
scene.add(stars2);


let ambient = new THREE.AmbientLight(0x222222);
scene.add(ambient);
//Sun
let sun, sun_geom, sun_mat;
sun_geom = new THREE.SphereGeometry(2300, 80, 80);
let sun_texture = new THREE.TextureLoader().load('textures/sun.png');
sun_texture.minFilter = THREE.LinearFilter;
sun_texture.anisotropy = 8;
sun_mat = new THREE.MeshPhongMaterial({map: sun_texture, emissiveMap: sun_texture, emissive: 0xffffff});

sun = new THREE.Mesh(sun_geom, sun_mat);
scene.add(sun);

//Mercury;
let mercury, mercury_geom, mercury_mat;
mercury_geom = new THREE.SphereGeometry(60, 20, 20);
let mercury_texture = new THREE.TextureLoader().load('textures/mercury.png');
mercury_texture.minFilter = THREE.LinearFilter;
mercury_texture.anisotropy = 8;
mercury_mat = new THREE.MeshPhongMaterial({map: mercury_texture});
mercury = new THREE.Mesh(mercury_geom, mercury_mat);
mercury.castShadow = true;
scene.add(mercury);
//Орбита
let mercury_orbit = new Orbit(8000);
mercury_orbit.draw(scene);

//Venus;
let venus, venus_geom, venus_mat;
venus_geom = new THREE.SphereGeometry(90, 20, 20);
let venus_texture = new THREE.TextureLoader().load('textures/venus.png');
venus_texture.minFilter = THREE.LinearFilter;
venus_texture.anisotropy = 8;
venus_mat = new THREE.MeshPhongMaterial({map: venus_texture});
venus = new THREE.Mesh(venus_geom, venus_mat);
venus.castShadow = true;
scene.add(venus);
//Орбита
let venus_orbit = new Orbit(17000);
venus_orbit.draw(scene);

//Earth;
let earth, earth_geom, earth_mat;
earth_geom = new THREE.SphereGeometry(100, 20, 20);
let earth_texture = new THREE.TextureLoader().load('textures/earth.png');
earth_texture.minFilter = THREE.LinearFilter;
earth_texture.anisotropy = 8;
earth_mat = new THREE.MeshPhongMaterial({map: earth_texture});
earth = new THREE.Mesh(earth_geom, earth_mat);
earth.castShadow = true;
scene.add(earth);
//Орбита
let earth_orbit = new Orbit(30000);
earth_orbit.draw(scene);

//Mars;
let mars, mars_geom, mars_mat;
mars_geom = new THREE.SphereGeometry(80, 20, 20);
let mars_texture = new THREE.TextureLoader().load('textures/mars.png');
mars_texture.minFilter = THREE.LinearFilter;
mars_texture.anisotropy = 8;
mars_mat = new THREE.MeshPhongMaterial({map: mars_texture});
mars = new THREE.Mesh(mars_geom, mars_mat);
mars.castShadow = true;
scene.add(mars);
//Орбита
let mars_orbit = new Orbit(50000);
mars_orbit.draw(scene);

//Jupiter;
let jupiter, jupiter_geom, jupiter_mat;
jupiter_geom = new THREE.SphereGeometry(350, 20, 20);
let jupiter_texture = new THREE.TextureLoader().load('textures/jupiter.png');
jupiter_texture.minFilter = THREE.LinearFilter;
jupiter_texture.anisotropy = 8;
jupiter_mat = new THREE.MeshStandardMaterial({map: jupiter_texture});
jupiter = new THREE.Mesh(jupiter_geom, jupiter_mat);
jupiter.castShadow = true;
jupiter.receiveShadow = false;
scene.add(jupiter);
//Орбита
let jupiter_orbit = new Orbit(74000);
jupiter_orbit.draw(scene);

//Saturn;
let saturn, saturn_geom, saturn_mat;
saturn_geom = new THREE.SphereGeometry(230, 20, 20);
let saturn_texture = new THREE.TextureLoader().load('textures/saturn.png');
saturn_texture.minFilter = THREE.LinearFilter;
saturn_texture.anisotropy = 8;
saturn_mat = new THREE.MeshPhongMaterial({map: saturn_texture});
saturn = new THREE.Mesh(saturn_geom, saturn_mat);
saturn.castShadow = true;
scene.add(saturn);
//Орбита
let saturn_orbit = new Orbit(98000);
saturn_orbit.draw(scene);

let ring_saturn_geom = new THREE.Geometry();
let ring_saturn_mat = new THREE.PointsMaterial({
    transparent: true,
    color: 0x808080,
    opacity: 0.3,
    size: 1,
    sizeAttenuation: false
});


for (let i = 0; i < 20000; i++) {
    let vertex = new THREE.Vector3();
    vertex.x = Math.sin(180/Math.PI * i) * (550 - i / 80);
    vertex.y = Math.random() * 20;
    vertex.z = Math.cos(180/Math.PI * i) * (550 - i / 80);
    ring_saturn_geom.vertices.push(vertex);
}

let ring = new THREE.Points(ring_saturn_geom, ring_saturn_mat);
ring.castShadow = true;
scene.add(ring);

//Uranus;
let uranus, uranus_geom, uranus_mat;
uranus_geom = new THREE.SphereGeometry(180, 20, 20);
let uranus_texture = new THREE.TextureLoader().load('textures/uranus.png');
uranus_texture.minFilter = THREE.LinearFilter;
uranus_texture.anisotropy = 8;
uranus_mat = new THREE.MeshPhongMaterial({map: uranus_texture});
uranus = new THREE.Mesh(uranus_geom, uranus_mat);
uranus.castShadow = true;
scene.add(uranus);
//Орбита
let uranus_orbit = new Orbit(120000);
uranus_orbit.draw(scene);

//Neptune;
let neptune, neptune_geom, neptune_mat;
neptune_geom = new THREE.SphereGeometry(180, 20, 20);
let neptune_texture = new THREE.TextureLoader().load('textures/neptune.png');
neptune_texture.minFilter = THREE.LinearFilter;
neptune_texture.anisotropy = 8;
neptune_mat = new THREE.MeshPhongMaterial({map: neptune_texture});
neptune = new THREE.Mesh(neptune_geom, neptune_mat);
neptune.castShadow = true;
scene.add(neptune);
//Орбита
let neptune_orbit = new Orbit(180000);
neptune_orbit.draw(scene);


let t = 0;
let y = 0;

document.addEventListener('mousemove', function (event) {
    y = parseInt(event.offsetY);

});

animate();

function animate() {
    requestAnimationFrame(animate);

        controls.update(0.002);
        sun.rotation.y += 0.001;
        earth.position.x = Math.sin(t * 0.1) * 30000;
        earth.position.z = Math.cos(t * 0.1) * 30000;

        mercury.position.x = Math.sin(t * 0.3) * 8000;
        mercury.position.z = Math.cos(t * 0.3) * 8000;

        venus.position.x = Math.sin(t * 0.2) * 17000;
        venus.position.z = Math.cos(t * 0.2) * 17000;

        mars.position.x = Math.sin(t * 0.08) * 50000;
        mars.position.z = Math.cos(t * 0.08) * 50000;

        jupiter.position.x = Math.sin(t * 0.06) * (-74000);
        jupiter.position.z = Math.cos(t * 0.06) * (-74000);

        saturn.position.x = Math.sin(t * 0.04) * 98000;
        saturn.position.z = Math.cos(t * 0.04) * 98000;

        ring.position.x = saturn.position.x;
        ring.position.z = saturn.position.z;

        uranus.position.x = Math.sin(t * 0.02) * 120000;
        uranus.position.z = Math.cos(t * 0.02) * 120000;

        neptune.position.x = Math.sin(t * 0.01) * 180000;
        neptune.position.z = Math.cos(t * 0.01) * 180000;

        earth.rotation.y += 0.01;
        mercury.rotation.y += 0.01;
        venus.rotation.y += 0.01;
        mars.rotation.y += 0.01;
        jupiter.rotation.y += 0.01;
        saturn.rotation.y += 0.01;
        ring.rotation.y -= 0.001;
        uranus.rotation.y += 0.01;
        neptune.rotation.y += 0.01;


    t += Math.PI / 180 * 2 * 0.1;
    renderer.render(scene, camera);
}


